#!/usr/bin/env python
"""
This sample application is a BACnet device that has one record access file
('file', 1) and one stream access file ('file', 2).
"""
import os
import random
import string
from bacpypes.debugging import bacpypes_debugging, ModuleLogger
from bacpypes.consolelogging import ConfigArgumentParser
from bacpypes.primitivedata import Real
from bacpypes.core import run
from bacpypes.app import BIPSimpleApplication
from bacpypes.errors import ExecutionError
from bacpypes.local.device import LocalDeviceObject
from bacpypes.local.file import LocalRecordAccessFileObject, LocalStreamAccessFileObject
from bacpypes.object import register_object_type, AnalogValueObject, Property
from bacpypes.primitivedata import Real
from bacpypes.service.file import FileServices
# some debugging
_debug = 0
_log = ModuleLogger(globals())
# configuration
RECORD_LEN = int(os.getenv('RECORD_LEN', 128))
RECORD_COUNT = int(os.getenv('RECORD_COUNT', 100))
OCTET_COUNT = int(os.getenv('OCTET_COUNT', 4096))
#   Local Record Access File Object Type
_debug = 0
_log = ModuleLogger(globals())
# globals
vendor_id = 999
#   RandomValueProperty
@bacpypes_debugging
class RandomValueProperty(Property):

    def __init__(self, identifier):
        if _debug: RandomValueProperty._debug("__init__ %r", identifier)
        Property.__init__(self, identifier, Real, default=None, optional=True, mutable=False)
        # writing to this property changes the multiplier
        self.multiplier = 100.0

    def ReadProperty(self, obj, arrayIndex=None):
        if _debug: RandomValueProperty._debug("ReadProperty %r arrayIndex=%r", obj, arrayIndex)
        # access an array
        if arrayIndex is not None:
            raise ExecutionError(errorClass='property', errorCode='propertyIsNotAnArray')
        # return a random value
        value = random.random() * self.multiplier
        if _debug: RandomValueProperty._debug("    - value: %r", value)
        return value
    def WriteProperty(self, obj, value, arrayIndex=None, priority=None, direct=False):
        if _debug: RandomValueProperty._debug("WriteProperty %r %r arrayIndex=%r priority=%r direct=%r", obj, value, arrayIndex, priority, direct)
        # change the multiplier
        self.multiplier = value
#   Vendor Analog Vlue Object Type
@bacpypes_debugging
class VendorAVObject(AnalogValueObject):
    objectType = 513
    properties = [
        RandomValueProperty(5504),
        ]
    def __init__(self, **kwargs):
        if _debug: VendorAVObject._debug("__init__ %r", kwargs)
        AnalogValueObject.__init__(self, **kwargs)

register_object_type(VendorAVObject, vendor_id=vendor_id)
@bacpypes_debugging
class TestStreamFile(LocalStreamAccessFileObject):
    def __init__(self, **kwargs):
        """ Initialize a stream accessed file object. """
        if _debug:
            TestStreamFile._debug("__init__ %r",
                kwargs,
                )
        LocalStreamAccessFileObject.__init__(self, **kwargs)
        # create some test data
        #file= open("/home/preetham/bacpypes/bacpypes-master/BACnet_rdf.rdf","r")
        file=open("C:\Users\Preetham\Documents\PreethamMasters\Fourth_semester\Thesis\semantic-interoperability-layer\BACnet_rdf.rdf")
        self._file_data = file.read()
        if _debug: TestStreamFile._debug("    - %d octets",
                len(self._file_data),
                )
    def __len__(self):
        """ Return the number of octets in the file. """
        if _debug: TestStreamFile._debug("__len__")
        return len(self._file_data)
    def read_stream(self, start_position, octet_count):
        """ Read a chunk of data out of the file. """
        if _debug: TestStreamFile._debug("read_stream %r %r",
                start_position, octet_count,
                )

        # end of file is true if last record is returned
        end_of_file = (start_position+octet_count) >= len(self._file_data)

        return end_of_file, \
            self._file_data[start_position:start_position + octet_count]

    def write_stream(self, start_position, data):
        """ Write a number of octets, starting at a specific offset. """
        if _debug: TestStreamFile._debug("write_stream %r %r",
                start_position, data,
                )
        # check for append
        if (start_position < 0):
            start_position = len(self._file_data)
            self._file_data += data
        # check to extend the file out to start_record records
        elif (start_position > len(self._file_data)):
            self._file_data += '\0' * (start_position - len(self._file_data))
            start_position = len(self._file_data)
            self._file_data += data

        # no slice assignment, strings are immutable
        else:
            data_len = len(data)
            prechunk = self._file_data[:start_position]
            postchunk = self._file_data[start_position + data_len:]
            self._file_data = prechunk + data + postchunk

        # return where the 'writing' actually started
        return start_position

#
#   __main__
#

def main():
    # parse the command line arguments
    args = ConfigArgumentParser(description=__doc__).parse_args()

    if _debug: _log.debug("initialization")
    if _debug: _log.debug("    - args: %r", args)

    # make a device object
    this_device = LocalDeviceObject(ini=args.ini)
    if _debug: _log.debug("    - this_device: %r", this_device)

    # make a sample application
    this_application = BIPSimpleApplication(this_device, args.ini.address)

    # add the capability to server file content
    this_application.add_capability(FileServices)

    # make a stream access file, add to the device
    f2 = TestStreamFile(
        objectIdentifier=('file', 2),
        objectName='StreamAccessFile2'
        )
    _log.debug("    - f2: %r", f2)
    this_application.add_object(f2)
    print(f2.__dict__)
    ravo1 = VendorAVObject(
        objectIdentifier=(513, 1), objectName='Random1'
        )
    this_application.add_object(ravo1)
    _log.debug("running")

    run()

    _log.debug("fini")

if __name__ == "__main__":
    main()
