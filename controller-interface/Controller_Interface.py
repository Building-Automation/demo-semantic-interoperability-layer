from http.server import BaseHTTPRequestHandler, HTTPServer
import itertools
import logging.config
import os
from urllib.parse import urlparse, parse_qs
import requests
from string import Template
from coapthon.client.helperclient import HelperClient
from coapthon.utils import create_logging
from coapthon.serializer import Serializer
import json
import re
import rdflib.plugins.sparql.parser
value = 0
coap_address = '192.168.178.54'
coap_path = '/temperature/fake/'
querystring=''
#SPARQL queries to extract devices details from datastore
testQuery = """PREFIX dogont: <http://elite.polito.it/ontologies/dogont.owl#> SELECT ?addressObject ?methodObject ?subject ?typeObject ?protocolObject WHERE{?subject dogont:isIn  dogont:Balcony . ?subject dogont:address ?addressObject . ?subject dogont:hasFunctionality ?typeObject . ?subject dogont:hasCommand ?methodObject . ?subject dogont:hasGateway ?protocolObject }"""
testQuery1 = """PREFIX dogont: <http://elite.polito.it/ontologies/dogont.owl#> SELECT ?addressObject ?methodObject ?subject ?typeObject ?protocolObject WHERE{?subject dogont:isIn  dogont:Lobby . ?subject dogont:address ?addressObject . ?subject dogont:hasFunctionality ?typeObject . ?subject dogont:hasCommand ?methodObject . ?subject dogont:hasGateway ?protocolObject }"""
testQuery2 = """PREFIX dogont: <http://elite.polito.it/ontologies/dogont.owl#> SELECT ?addressObject ?methodObject ?subject ?typeObject ?protocolObject WHERE{?subject dogont:isIn  dogont:Conference_Room . ?subject dogont:address ?addressObject . ?subject dogont:isA ?typeObject . ?subject dogont:hasCommand ?methodObject . ?subject dogont:hasGateway ?protocolObject }"""
readvalue_lobby="""PREFIX dogont: <http://elite.polito.it/ontologies/dogont.owl#> SELECT ?addressObject ?component ?subject where { ?subject dogont:isIn dogont:Lobby . ?subject dogont:hasGateway ?component . ?subject dogont:hasCommand ?command . ?subject dogont:address ?addressObject } """
readvalue_balcony="""PREFIX dogont: <http://elite.polito.it/ontologies/dogont.owl#> SELECT ?addressObject ?component ?subject ?command where { ?subject dogont:isIn dogont:Balcony . ?subject dogont:hasGateway ?component . ?subject dogont:hasCommand ?command . ?subject dogont:address ?addressObject } """
updatestring_BACnet='<real name="value" href="value/" val="0.7257863879203796" writable="true" min="0.0" max="100.0"/>'
#updatestring_DPWS='<?xml version="1.0" encoding="UTF-8"?><s12:Envelope xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01" xmlns:s12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing"><s12:Header><wsa:Action>http://telecom-sudparis.eu/operations/SwitchOn</wsa:Action><wsa:MessageID>urn:uuid:8843b510-d9fe-11e9-bf12-f69a076842d0</wsa:MessageID><wsa:To>http://localhost:4567/L1568799626404Service</wsa:To></s12:Header><s12:Body><n1:param n3:type="n2:string" xmlns:n1="http://telecom-sudparis.eu" xmlns:n2="http://www.w3.org/2001/XMLSchema" xmlns:n3="http://www.w3.org/2001/XMLSchema-instance" /></s12:Body></s12:Envelope>'
if not os.path.isfile("logging.conf"):
    create_logging()

logger = logging.getLogger(__name__)
logging.config.fileConfig("logging.conf", disable_existing_loggers=False)
#web-server class
class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
#GET request method
    def do_GET(self):
        self._set_headers()
        # print self.requestline
        url_string = self.requestline

        parsed = urlparse(url_string)
        querystring = parse_qs(parsed.query)['query']
        print(querystring)
        #extract information about devices present in Lobby
        if (querystring[0] == 'Lobby'):
          value=  SPARQL_client(testQuery1)
          self.wfile.write(value)
        #extract information about devices present in Balcony
        elif (querystring[0] == 'Balcony'):
           value= SPARQL_client(testQuery)
           self.wfile.write(value)
           #extract information about devices present in Conference room
        elif (querystring[0] =='Conference_Room'):
            value=SPARQL_client(testQuery2)
            self.wfile.write(value)
        #else clause to perform a device level Read operation
        else:
            url_test=querystring[0]
            url_test=querystring[0]
            value=url_test.split('+')
            protocol=value[0]
            address=value[1]
            #read a value from BACnet device
            if (protocol=="BACnetComponent"):
                add='http://localhost:9000/read/'+address
                r = requests.get(add)
                print(r.text)
                self.wfile.write(r.content)
            else:
                #read a value from CoAP device
                value=Coap_client()
                print(value)
                self.wfile.write(bytes(value, "utf8"))

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
          #set the headers for POST message
        self._set_headers()
        url_string = self.requestline
        parsed = urlparse(url_string)
        querystring =parse_qs(parsed.query)['query']

        url_test=querystring[0]
        value=url_test.split('+')

        protocol=value[0]
        address=value[1]
        update_value=value[2]
          #method to perform operation based on SPARQL queries
        if(protocol=="SPARQL"):
           type=value[2]
           rw=value[3]
            #device level operation using SPARQL queries
           if(type=="query" and rw=="set"):
                location=value[4]
                print(location)
                if(location=="Lobby"):
                    answer=SPARQL_client(readvalue_lobby)
                    answer2=json.loads(answer)
                    address=answer2['results']['bindings'][0]['addressObject']['value']
                    component=answer2['results']['bindings'][0]['component']['value']
                    print(address, component)
                    if(component=='http://elite.polito.it/ontologies/dogont.owl#BACnetComponent'):
                        address_new="http://localhost:9000/read/"+address
                        r = requests.get(address_new)
                        self.wfile.write(r.content)
                else:
                    answer=SPARQL_client(readvalue_balcony)
                    answer2=json.loads(answer)
                    address=answer2['results']['bindings'][0]['addressObject']['value']
                    component=answer2['results']['bindings'][0]['component']['value']
                    if(component=='http://elite.polito.it/ontologies/dogont.owl#CoAPComponent'):
                        value=Coap_client()
                        print(value)
                        self.wfile.write(bytes(value, "utf8"))
        #device level update operation (write request) using SPARQL update queries
           elif(type=="update" and rw=="set"):
                subject=value[4]
                object=value[5]
                object_new=re.findall(r'"([^"]*)"',object)
                update_query=Template("""PREFIX dogont: <http://elite.polito.it/ontologies/dogont.owl#> SELECT ?addressObject ?component ?command where { <$subject> dogont:hasGateway ?component . <$subject> dogont:hasCommand ?command . <$subject> dogont:address ?addressObject } """)
                answer=SPARQL_client(update_query.substitute(subject=subject))
                answer2=json.loads(answer)
                component=answer2['results']['bindings'][0]['component']['value']
                addressObject=answer2['results']['bindings'][0]['addressObject']['value']
                command=[answer2['results']['bindings'][0]['command']['value'],answer2['results']['bindings'][1]['command']['value']]
                print(object_new[0])
                if(command[0]=='http://elite.polito.it/ontologies/dogont.owl#writeProperty' or command[0]=='http://elite.polito.it/ontologies/dogont.owl#POST' or command[1]=='http://elite.polito.it/ontologies/dogont.owl#writeProperty' or command[1]=='http://elite.polito.it/ontologies/dogont.owl#POST' ):
                    print("hello")
                    print(component)
                    if(component=='http://elite.polito.it/ontologies/dogont.owl#BACnetComponent'):
                      address_new="http://localhost:9000/write/"+addressObject+"/"+object_new[0]
                      r = requests.post(address_new)
                      self.wfile.write(r.content)
                else:
                    self.wfile.write("Operation not permitted")
           #query operation
           elif(type=="query" and rw=="noset"):
                answer=SPARQL_client(value[1])
                answer2=json.loads(answer)
                print(answer2)
                out=str(answer2['results']['bindings'][0])
                self.wfile.write(bytes(out,"utf8"))
           elif(type=="update" and rw=="noset"):
                answer=SPARQL_client(value[1])
                answer2=json.loads(answer)
                out=str(answer2['results']['bindings'][0])
                self.wfile.write(bytes(out,"utf8"))
        elif(protocol=="DPWSComponent"):
            service_address=value[1]
            service_operation=value[2]
            output=DPWS_client(service_address, service_operation)
            self.wfile.write(output)
        elif(protocol=="BACnetComponent"):
                if(update_value=="SwitchOn"):
                    update_value_new=100
                    add='http://localhost:9000/write/'+address+"/"+str(update_value_new)
                    r = requests.post(add)
                    print(r.text)
                    self.wfile.write(r.content)
                else:
                    update_value_new=0
                    add='http://localhost:9000/write/'+address+"/"+str(update_value_new)
                    r = requests.post(add)
                    print(r.text)
                    self.wfile.write(r.content)



def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', 8008)
    httpd = server_class(server_address, handler_class)
    print('Starting httpd...')
    httpd.serve_forever()

#CoAP gateway
def Coap_client():

    print("hello from coap client class")
    print(coap_address)
    print(coap_path)
    client = HelperClient(server=(coap_address, 5683))
    response = client.get(coap_path)
    print(response)
    client.stop()
    return response._payload

#DPWS gateway
def DPWS_client(x,y):
    print(x)
    xml_1 = """<?xml version="1.0" encoding="UTF-8"?><s12:Envelope xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01" xmlns:s12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing"><s12:Header><wsa:Action>http://telecom-sudparis.eu/operations/SwitchOn</wsa:Action><wsa:MessageID>urn:uuid:fd8e89d0-d86e-11e9-bfc3-324efc9da104</wsa:MessageID><wsa:To>http://localhost:4567/L1568630479580Service</wsa:To></s12:Header><s12:Body><n1:param n3:type="n2:string" xmlns:n1="http://telecom-sudparis.eu" xmlns:n2="http://www.w3.org/2001/XMLSchema" xmlns:n3="http://www.w3.org/2001/XMLSchema-instance" /></s12:Body></s12:Envelope>"""
    xml_2 = """<?xml version="1.0" encoding="UTF-8"?><s12:Envelope xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01" xmlns:s12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing"><s12:Header><wsa:Action>http://telecom-sudparis.eu/operations/SwitchOff</wsa:Action><wsa:MessageID>urn:uuid:fd8e89d0-d86e-11e9-bfc3-324efc9da104</wsa:MessageID><wsa:To>http://localhost:4567/L1568630479580Service</wsa:To></s12:Header><s12:Body><n1:param n3:type="n2:string" xmlns:n1="http://telecom-sudparis.eu" xmlns:n2="http://www.w3.org/2001/XMLSchema" xmlns:n3="http://www.w3.org/2001/XMLSchema-instance" /></s12:Body></s12:Envelope>"""
    if(y=='SwitchOn'):
        print("Hello from Switch On")
        headers = {'Content-Type': 'application/soap+xml'}  # set what your server accepts
        out= requests.post(x, data=xml_1, headers=headers).text
        print(out)
    else:
        print("Hello from Switch Off")
        headers = {'Content-Type': 'application/soap+xml'}  # set what your server accepts
        out=requests.post(x, data=xml_2, headers=headers).text
        print(out)

def SPARQL_client(parameters):
    endpoint = 'http://localhost:3030/room'
    statement = parameters
    r = requests.get(endpoint, params={'format': 'json', 'query': statement})
    return r.content


if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
