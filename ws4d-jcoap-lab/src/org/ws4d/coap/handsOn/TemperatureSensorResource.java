package org.ws4d.coap.handsOn;

import java.io.IOException;
import java.util.List;

import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapData;

/**
 * A CoAP Resource representing the temperature measurement of a one wire sensor
 */
public class TemperatureSensorResource extends BasicCoapResource {
	
	/** The index of the sensor to be read */
	private int sensor;

	private TemperatureSensorResource(String path, String value, CoapMediaType mediaType, int sensor) {
		// Invoke super class constructor (constructor of BasicCoapResource) 
		super(path, value, mediaType);
		
		// Store the index of the sensor to be provided
		this.sensor = sensor;
		
		//Disallow POST, PUT and DELETE
		this.setDeletable(false);
    	this.setPostable(false);
    	this.setPutable(false);
    	
    	// Add some meta Information (optional)
    	this.setResourceType("Temperature");
    	this.setInterfaceDescription("GET");
    	
    	
	}
    
    public TemperatureSensorResource(){
    	// 		resource path,			initial value,	media type					Sensor index
    	this(	"/temperature/sensor", 	"0.0", 			CoapMediaType.text_plain, 	0);
    }

    
    public synchronized CoapData get(List<String> query, List<CoapMediaType> mediaTypesAccepted) {
    	/* Just ignore query parameters for this example.*/
    	return get(mediaTypesAccepted);
    }
}
