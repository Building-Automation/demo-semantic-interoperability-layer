package org.ws4d.coap.handsOn;

import java.util.List;
import java.util.Locale;

import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapData;

public class TemperatureFakeResource extends BasicCoapResource {
	
	private static double MAX_TEMP = 30.0;
	private static double MIN_TEMP = 20.0;

	private TemperatureFakeResource(String path, String value, CoapMediaType mediaType) {
		// Invoke super class constructor (constructor of BasicCoapResource) 
		super( path, value, mediaType);
		
		//Disallow POST, PUT and DELETE operation on this resource
		this.setDeletable(false);
    	this.setPostable(false);
    	this.setPutable(false);
    	
    	// Add some meta information (optional)
    	this.setResourceType("Temperature");
    	this.setInterfaceDescription("GET only");
	}
    
    public TemperatureFakeResource(){
    	// 		resource path,			initial value,	media type
    	this(	"/temperature/fake", 	"0.0",			CoapMediaType.text_plain);
    }

    @Override
    public synchronized CoapData get(List<CoapMediaType> mediaTypesAccepted) {
    	double randomTemperature = MIN_TEMP + ( Math.random() * (MAX_TEMP-MIN_TEMP) );
    	String result = String.format(Locale.US, "%1$,.1f", randomTemperature);
    	return new CoapData(result, CoapMediaType.text_plain);
    }
    
    @Override
    public synchronized CoapData get(List<String> query, List<CoapMediaType> mediaTypesAccepted) {
    	/* Just ignore query parameters for this example.*/
    	return get(mediaTypesAccepted);
    }
}
