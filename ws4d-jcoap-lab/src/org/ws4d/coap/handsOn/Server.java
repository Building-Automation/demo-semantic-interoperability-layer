package org.ws4d.coap.handsOn;

import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapResourceServer;
import org.ws4d.coap.core.rest.api.CoapResource;

public class Server {
	private CoapResourceServer resourceServer;
	public static void main(String[] args) {
		new Server().startServer();
	}
	public void startServer() {
		if (this.resourceServer != null){
			this.resourceServer.stop();
		}
		/* FIXME 1.1 Instantiate a new "CoapResourceServer" */
		this.resourceServer = new CoapResourceServer();
		/* FIXME 1.2 Instantiate a resource */
		CoapResource resource = new TemperatureFakeResource();
		//CoapResource resource1= new TemperatureFakeResource2();
		/* FIXME 1.3 Add the resource to the "CoapResourceServer" */
		this.resourceServer.createResource(resource);
		/* TODO 5.1 Add another resource*/
		//this.resourceServer.createResource( resource1);
		/* Start the server */
		try {
			/* FIXME 1.4 Run the server */
			this.resourceServer.start(CoapConstants.COAP_DEFAULT_PORT);	
			System.out.println("=== Server Running ===");
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(1);
		}
	}
}
