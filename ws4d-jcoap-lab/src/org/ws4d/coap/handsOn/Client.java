package org.ws4d.coap.handsOn;

import java.net.InetAddress;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;

public class Client implements CoapClient {

	/** A manager to keep track of our connections */
	private CoapChannelManager channelManager;

	/** The target where we want to send our request to */
	private CoapClientChannel clientChannel;

	public static void main(String[] args) {
		Client coapClient = new Client();
		System.out.println("=== START Client ===");
		coapClient.start("10.40.143.93", CoapConstants.COAP_DEFAULT_PORT);
		
	}

	/** The method containing the creation of our requests */
	@SuppressWarnings("deprecation")
	public void start(String serverAddress, int serverPort) {

		/* FIXME 2.1 Get the "ChannelManager" instance */
		this.channelManager = BasicCoapChannelManager.getInstance();

		this.clientChannel = null;

		try {
			/* FIXME 2.2 Create a channel to the server */
			this.clientChannel = this.channelManager.connect(this, InetAddress.getByName(serverAddress), serverPort);

			/* Make sure the channel is not null */
			if (this.clientChannel == null) {
				System.err.println("Connect failed: clientChannel is null!");
				System.exit(-1);
			}
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(-1);
		}

		/* FIXME 2.3 Create a request */
		CoapRequest request = clientChannel.createRequest(true, CoapRequestCode.PUT);
		request.setUriPath("/BACnetE-Lab/555/AnalogValue1/value");
		request.setContentType(CoapMediaType.xml);
		request.setPayload("<real name=\"value\" href=\"value/\" val=\"50.0\" writable=\"true\" min=\"0.0\" max=\"100.0\"/>".getBytes());
		this.clientChannel.sendMessage(request);
	}

	/*
	 * *************************************************************************
	 * Some callback functions that are invoked by the jCoAP-Framework
	 ***************************************************************************
	 */

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		System.err.println("Connection Failed");
		System.exit(-1);
	}

	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		
		// Print out the response
		String responseMessage = "Response: " + response.toString();
		if (response.getPayload() != null) {
			responseMessage += " Payload: " + new String(response.getPayload());

			/* TODO 5.2 Add some behavior. */
			/* Get the float value from the response payload */
			// float temperature = Float.parseFloat(response.??);

			/* Determine the new state to be sent to the server */

			/* Create a PUT request to "/ACControl" */

			/* Set media-type and payload of the request */

			/* Send the request */

		}
		System.out.println(responseMessage);	
		// TODO 4.1 Comment out, to prevent the client to exit on a response.
		
		System.out.println("=== STOP otha Client ===");
		
		//System.exit(0);
	}

	@Override
	public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
		/* Ignore incoming multicast messages */
	}
}
