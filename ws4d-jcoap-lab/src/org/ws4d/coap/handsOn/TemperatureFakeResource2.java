package org.ws4d.coap.handsOn;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapData;

public class TemperatureFakeResource2 extends BasicCoapResource {

	private static double MAX_TEMP = 30.0;
	private static double MIN_TEMP = 20.0;
	public static String contents;
	private TemperatureFakeResource2(String path, String value, CoapMediaType mediaType) {
		// Invoke super class constructor (constructor of BasicCoapResource) 
		super( path, value, mediaType);
		
		//Disallow POST, PUT and DELETE operation on this resource
		this.setDeletable(false);
    	this.setPostable(false);
    	this.setPutable(false);
    	
    	// Add some meta information (optional)
    	this.setResourceType("Temperature2");
    	this.setInterfaceDescription("GET only");
	}
    
    public TemperatureFakeResource2(){
    	// 		resource path,			initial value,	media type
    	this(	"/temperature2/fake", 	"0.0",			CoapMediaType.xml);
    }

    @Override
    public synchronized CoapData get(List<CoapMediaType> mediaTypesAccepted) {
    	final String inFile = "/home/pi/CoAP_v4.rdf";
    	//final String inFile = "C:\\Users\\Preetham\\Documents\\PreethamMasters\\Fourth_semester\\Thesis\\semantic-interoperability-layer\\Automatic_mapping_v2\\Coap.rdf";
		try{
			contents = new String(Files.readAllBytes(Paths.get(inFile)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return new CoapData(contents, CoapMediaType.xml);
    }
    
    @Override
    public synchronized CoapData get(List<String> query, List<CoapMediaType> mediaTypesAccepted) {
    	/* Just ignore query parameters for this example.*/
    	return get(mediaTypesAccepted);
    }
}
