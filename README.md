# Demo - Semantic Interoperability Layer

Demo of the Semantic Interoperability Layer for Building Automation Networks created in the BBSR project "Open-BIM - Erweiterung um Gebäudeautomation und Smart Home"

**Preparation**

*  All steps below assume a setup with one PC and one Raspberry Pi
*  Connect the PC and Raspberry Pi to the same subnetwork/LAN
*  Python 3 and a recent Java runtime need to be present on both machines

**Raspberry Pi: Steps to run CoAP Server**

*  Deploy the folder "ws4d-jcoap-lab" to /home/pi/
*  Navigate to /home/pi/ws4d-jcoap-lab
*  Run the program "CoAP_Server.jar"

**Raspberry Pi: Steps to run BACnet Server**

*  To install bacpypes on the Pi, run "pip3 install bacpypes" (only needed once)
*  Deploy the folder "bacpypes-master_server" to /home/pi/
*  Navigate to /home/pi/bacpypes-master_server
*  Modify the file "BACpypes.ini" to set the Pi's current IP and subnet address, use subnet broadcast address for BBMD
*  Run the program "server_fileobject.py" (reads device description file "BACnet_device.rdf")

**PC: Steps to start Apache Jena Fuseki Server (RDF/SPARQL endpoint)**

*  Set the IP addresses of the devices "TemperatureSensor1" (BACnet server on Pi) and "TemperatureSensor2" (CoAP server on Pi) in the ontology file "Room_v3.rdf"
*  Navigate to folder "apache-jena-fuseki-3.12.0"
*  Run the program "fuseki-server.jar"
*  Enter URL "http://localhost:3030" in a browser
*  Navigate to tab "dataset->upload files" and upload the ontology file "Room_v3.rdf"

**PC: Steps to run BACnet Gateway**

*  To install bacpypes on the PC, run "pip3 install bacpypes" (only needed once)
*  Modify the file "BACpypes.ini" to set the PCs current IP and subnet address, use subnet broadcast address for BBMD
*  Navigate to folder "bacpypes-master_gateway"
*  Run the program "BACnet_Gateway.py" to start the BACnet gateway application

**PC: Steps to run Controller Interface Application**

1.  As a prerequisite, install the below python packages:
* pip3 install rdflib
* pip3 install CoAPthon3
* pip3 install httpserver
* pip3 install requests

2.	Navigate to folder "controller-interface"
3.  Modify the file "Controller_Interface.py" and set the CoAP server IP address (variable "coap_address")
4.  Start the program "Controller_Interface.py"
5.  Open the file "Floor-Plan.html" in a browser to interact with the devices
