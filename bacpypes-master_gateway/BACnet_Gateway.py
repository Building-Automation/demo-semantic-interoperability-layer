#!/usr/bin/python

"""
HTTPServer
"""

import json
import threading
from collections import OrderedDict
import sys
import bacpypes
from bacpypes.apdu import ReadPropertyRequest, WhoIsRequest
from bacpypes.app import BIPSimpleApplication
from bacpypes.consolelogging import ConfigArgumentParser
from bacpypes.constructeddata import Array
from bacpypes.core import run, deferred
from bacpypes.debugging import class_debugging, ModuleLogger
from bacpypes.iocb import IOCB
from bacpypes.local.device import LocalDeviceObject
from bacpypes.object import get_object_class, get_datatype
from bacpypes.pdu import Address, GlobalBroadcast
from bacpypes.primitivedata import Unsigned, ObjectIdentifier
from urllib.parse import urlparse, parse_qs
from http.server import BaseHTTPRequestHandler, HTTPServer
import socketserver
from bacpypes.pdu import Address
from bacpypes.object import get_datatype

from bacpypes.apdu import SimpleAckPDU, \
    ReadPropertyRequest, ReadPropertyACK, WritePropertyRequest
from bacpypes.primitivedata import Null, Atomic, Boolean, Unsigned, Integer, \
    Real, Double, OctetString, CharacterString, BitString, Date, Time, ObjectIdentifier
from bacpypes.constructeddata import Array, Any, AnyAtomic
# some debugging
_debug = 0
_log = ModuleLogger(globals())

# reference a simple application
this_application = None
server = None



#
#   ThreadedHTTPRequestHandler
#

@class_debugging
class ThreadedHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if _debug: ThreadedHTTPRequestHandler._debug("do_GET")


        # get the thread
        cur_thread = threading.current_thread()
        if _debug: ThreadedHTTPRequestHandler._debug("    - cur_thread: %r", cur_thread)

        # parse query data and params to find out what was passed
        parsed_params = urlparse(self.path)
        if _debug: ThreadedHTTPRequestHandler._debug("    - parsed_params: %r", parsed_params)
        parsed_query = parse_qs(parsed_params.query)
        if _debug: ThreadedHTTPRequestHandler._debug("    - parsed_query: %r", parsed_query)

        # find the pieces
        args = parsed_params.path.split('/')
        if _debug: ThreadedHTTPRequestHandler._debug("    - args: %r", args)

        if (args[1] == 'read'):
            output=self.do_read(args[2:])
            output=str(output)
            self.send_response(200)
            self.send_header("Content-type", 'text')
            self.end_headers()
            self.wfile.write(bytes(output,"utf8"))
        elif (args[1] == 'whois'):
            self.do_whois(args[2:])



        else:
            return "'read' or 'whois' expected"
    def do_POST(self):
        parsed_params = urlparse(self.path)
        parsed_query = parse_qs(parsed_params.query)
        args = parsed_params.path.split('/')
        if (args[1] == 'write'):
            output=self.do_write(args[2:])
            print(output)
            self.send_response(200)
            self.send_header("Content-type", 'text')
            self.end_headers()
            self.wfile.write(output)
    def do_read(self, args):
        if _debug: ThreadedHTTPRequestHandler._debug("do_read %r", args)

        try:
            addr, obj_id = args[:2]
            print(args[:2])
            print(addr)
            obj_id = ObjectIdentifier(obj_id).value
            print(obj_id)
            # get the object type
            if not get_object_class(obj_id[0]):
                raise ValueError("unknown object type")

            # implement a default property, the bain of committee meetings
            if len(args) == 3:
                prop_id = args[2]
            else:
                prop_id = "presentValue"

            # look for its datatype, an easy way to see if the property is
            # appropriate for the object
            datatype = get_datatype(obj_id[0], prop_id)
            if not datatype:
                raise ValueError("invalid property for object type")

            # build a request
            request = ReadPropertyRequest(
                objectIdentifier=obj_id,
                propertyIdentifier=prop_id,
                )
            request.pduDestination = Address(addr)

            # look for an optional array index
            if len(args) == 5:
                request.propertyArrayIndex = int(args[4])
            if _debug: ThreadedHTTPRequestHandler._debug("    - request: %r", request)

            # make an IOCB
            iocb = IOCB(request)
            if _debug: ThreadedHTTPRequestHandler._debug("    - iocb: %r", iocb)

            # give it to the application
            deferred(this_application.request_io, iocb)

            # wait for it to complete
            iocb.wait()

            # filter out errors and aborts
            if iocb.ioError:
                if _debug: ThreadedHTTPRequestHandler._debug("    - error: %r", iocb.ioError)
                result = { "error": str(iocb.ioError) }
            else:
                if _debug: ThreadedHTTPRequestHandler._debug("    - response: %r", iocb.ioResponse)
                apdu = iocb.ioResponse

                # find the datatype
                datatype = get_datatype(apdu.objectIdentifier[0], apdu.propertyIdentifier)
                if _debug: ThreadedHTTPRequestHandler._debug("    - datatype: %r", datatype)
                if not datatype:
                    raise TypeError("unknown datatype")

                # special case for array parts, others are managed by cast_out
                if issubclass(datatype, Array) and (apdu.propertyArrayIndex is not None):
                    if apdu.propertyArrayIndex == 0:
                        datatype = Unsigned
                    else:
                        datatype = datatype.subtype
                    if _debug: ThreadedHTTPRequestHandler._debug("    - datatype: %r", datatype)

                # convert the value to a dict if possible
                value = apdu.propertyValue.cast_out(datatype)


                if hasattr(value, 'dict_contents'):
                    value = value.dict_contents(as_class=OrderedDict)
                if _debug: ThreadedHTTPRequestHandler._debug("    - value: %r", value)

                result = { "value": value }
                return value
        except Exception as err:
            ThreadedHTTPRequestHandler._exception("exception: %r", err)
            result = { "exception": str(err) }

        # write the result
        #json.dump(result, self.wfile)
        self.wfile.write(json.dumps(result).encode())
    def do_write(self, args):
        """write <addr> <objid> <prop> <value> [ <indx> ] [ <priority> ]"""
        ThreadedHTTPRequestHandler._debug("do_write %r", args)

        try:
            addr, obj_id, prop_id = args[:3]
            obj_id = ObjectIdentifier(obj_id).value
            value = args[3]

            indx = None
            if len(args) >= 5:
                if args[4] != "-":
                    indx = int(args[4])
            if _debug: ThreadedHTTPRequestHandler._debug("    - indx: %r", indx)

            priority = None
            if len(args) >= 6:
                priority = int(args[5])
            if _debug: ThreadedHTTPRequestHandler._debug("    - priority: %r", priority)

            # get the datatype
            datatype = get_datatype(obj_id[0], prop_id)
            if _debug: ThreadedHTTPRequestHandler._debug("    - datatype: %r", datatype)

            # change atomic values into something encodeable, null is a special case
            if (value == 'null'):
                value = Null()
            elif issubclass(datatype, AnyAtomic):
                dtype, dvalue = value.split(':', 1)
                if _debug: ThreadedHTTPRequestHandler._debug("    - dtype, dvalue: %r, %r", dtype, dvalue)

                datatype = {
                    'b': Boolean,
                    'u': lambda x: Unsigned(int(x)),
                    'i': lambda x: Integer(int(x)),
                    'r': lambda x: Real(float(x)),
                    'd': lambda x: Double(float(x)),
                    'o': OctetString,
                    'c': CharacterString,
                    'bs': BitString,
                    'date': Date,
                    'time': Time,
                    'id': ObjectIdentifier,
                    }[dtype]
                if _debug: ThreadedHTTPRequestHandler._debug("    - datatype: %r", datatype)

                value = datatype(dvalue)
                if _debug: ThreadedHTTPRequestHandler._debug("    - value: %r", value)

            elif issubclass(datatype, Atomic):
                if datatype is Integer:
                    value = int(value)
                elif datatype is Real:
                    value = float(value)
                elif datatype is Unsigned:
                    value = int(value)
                value = datatype(value)
            elif issubclass(datatype, Array) and (indx is not None):
                if indx == 0:
                    value = Integer(value)
                elif issubclass(datatype.subtype, Atomic):
                    value = datatype.subtype(value)
                elif not isinstance(value, datatype.subtype):
                    raise TypeError("invalid result datatype, expecting %s" % (datatype.subtype.__name__,))
            elif not isinstance(value, datatype):
                raise TypeError("invalid result datatype, expecting %s" % (datatype.__name__,))
            if _debug: ThreadedHTTPRequestHandler._debug("    - encodeable value: %r %s", value, type(value))

            # build a request
            request = WritePropertyRequest(
                objectIdentifier=obj_id,
                propertyIdentifier=prop_id
                )
            request.pduDestination = Address(addr)

            # save the value
            request.propertyValue = Any()
            try:
                request.propertyValue.cast_in(value)
            except Exception as error:
                ThreadedHTTPRequestHandler._exception("WriteProperty cast error: %r", error)

            # optional array index
            if indx is not None:
                request.propertyArrayIndex = indx

            # optional priority
            if priority is not None:
                request.priority = priority

            if _debug: ThreadedHTTPRequestHandler._debug("    - request: %r", request)

            # make an IOCB
            iocb = IOCB(request)
            if _debug: ThreadedHTTPRequestHandler._debug("    - iocb: %r", iocb)

            # give it to the application
            deferred(this_application.request_io, iocb)

            # wait for it to complete
            iocb.wait()

            # do something for success
            if iocb.ioResponse:
                # should be an ack
                if not isinstance(iocb.ioResponse, SimpleAckPDU):
                    if _debug: ThreadedHTTPRequestHandler._debug("    - not an ack")
                    return

                sys.stdout.write("ack\n")

                self.send_response(200)
                self.send_header("Content-type", 'text')
                self.end_headers()
                self.wfile.write('ack')
            # do something for error/reject/abort
            if iocb.ioError:
                sys.stdout.write(str(iocb.ioError) + '\n')

        except Exception as error:
            ThreadedHTTPRequestHandler._exception("exception: %r", error)
    def do_whois(self, args):
        if _debug: ThreadedHTTPRequestHandler._debug("do_whois %r", args)

        try:
            # build a request
            request = WhoIsRequest()
            if (len(args) == 1) or (len(args) == 3):
                request.pduDestination = Address(args[0])
                del args[0]
            else:
                request.pduDestination = GlobalBroadcast()

            if len(args) == 2:
                request.deviceInstanceRangeLowLimit = int(args[0])
                request.deviceInstanceRangeHighLimit = int(args[1])
            if _debug: ThreadedHTTPRequestHandler._debug("    - request: %r", request)

            # make an IOCB
            iocb = IOCB(request)
            if _debug: ThreadedHTTPRequestHandler._debug("    - iocb: %r", iocb)

            # give it to the application
            this_application.request_io(iocb)

            # no result -- it would be nice if these were the matching I-Am's
            result = {}

        except Exception as err:
            ThreadedHTTPRequestHandler._exception("exception: %r", err)
            result = { "exception": str(err) }

        # write the result
        #json.dump(result, self.wfile)
        self.wfile.write(json.dumps(result).encode())



class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

#
#   __main__
#

try:
    # parse the command line arguments
    parser = ConfigArgumentParser(description=__doc__)

    # add an option to override the port in the config file
    parser.add_argument('--port', type=int,
        help="override the port in the config file to PORT",
        default=9000,
        )
    args = parser.parse_args()

    if _debug: _log.debug("initialization")
    if _debug: _log.debug("    - args: %r", args)

    # make a device object
    this_device = LocalDeviceObject(ini=args.ini)
    if _debug: _log.debug("    - this_device: %r", this_device)

    # make a simple application
    this_application = BIPSimpleApplication(this_device, args.ini.address)

    # local host, special port
    HOST, PORT = "", int(args.port)
    server = ThreadedTCPServer((HOST, PORT), ThreadedHTTPRequestHandler)
    if _debug: _log.debug("    - server: %r", server)

    # Start a thread with the server -- that thread will then start a thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    if _debug: _log.debug("    - server_thread: %r", server_thread)

    # exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()

    if _debug: _log.debug("running")

    run()

except Exception as err:
    _log.exception("an error has occurred: %s", err)

finally:
    if server:
        server.shutdown()

    if _debug: _log.debug("finally")
